<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--

    Spring Bloom by christopher robinson
    http://www.edg3.co.uk/
    hope you enjoy it and find it usefull :)

-->
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="<?php print $language->language; ?>"
      dir="<?php print $language->dir; ?>" >

  <head profile="<?php print $grddl_profile; ?>" >
    <title><?php print $head_title; ?></title>    
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>

  <body class="<?php print "$classes $attributes"; ?>" >
    <?php print $page_top; ?>
    <?php print $page; ?>
    <?php print $page_bottom; ?>
  </body>

</html>

