
<div id="body">

  <div id="header">
    <h1><?php print $site_name; ?></h1>

  <?php if( !empty($site_slogan)) : ?>
    <p id="site-slogan"><?php print $site_slogan; ?></p>
  <?php endif; ?>

  <?php if(isset($main_menu)) : ?>
    <div id="primary_links">
      <?php print theme("links__system_main_menu", array(
          'links' => $main_menu,
          'attributes' => array(
              'id' => 'main-menu',
              'class' => array('links', 'clearfix'),
              'heading' => t('Main menu'),
          ),
      )); ?>
    </div>
  <?php endif; ?>
  
  <?php if(isset($secondary_menu)) : ?>
    <div id="secondary_links">
      <?php print theme("links__system_secondary_menu", array(
          'links' => $secondary_menu,
          'attributes' => array(
              'id' => 'secondary-menu',
              'class' => array('links', 'clearfix'),
              'heading' => t('Secondary menu'),
          ),
      )); ?>
    </div>
  <?php endif; ?>

  </div>
  
  <div id="content">
  <?php if(isset($page['sidebar_first'])) : ?>
    <div id="sidebar">
      <?php print render($page['sidebar_first']); ?>
    </div>
  <?php endif; ?>
  
    <div id="main">
    <?php if ( !empty($page['highlighted'])) : ?>
      <div id="highlighted"><?php print render($page['highlighted']); ?></div>
    <?php endif; ?>

    <?php if ( !empty($page['header']) OR !empty($breadcrumb)) : ?>
      <div id="header-region">
        <?php print $breadcrumb; ?>
  	    <?php print render($page['header']); ?>
      </div>
    <?php endif; ?>
  
    <?php if ( !empty($title)) : ?>
      <h2 class="content-title">
        <?php print $title; ?>
      </h2>
    <?php endif; ?>
  
    <?php if ( !empty($tabs)) : ?>
      <?php print render($tabs); ?>
    <?php endif; ?>
  
    <?php if ( !empty($mission)) : ?>
      <p id="mission"> <?php print $mission; ?> </p>
    <?php endif; ?>
  
    <?php if ( !empty($page['help'])) : ?>
      <p id="help"> <?php print render($page['help']); ?> </p>
    <?php endif; ?>
  
    <?php if ( !empty($messages)) : ?>
      <div id="message"> <?php print $messages; ?> </div>
    <?php endif; ?>
  
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>
  </div>
  
  <div id="footer">
    <?php print render($page['footer']); ?>
  </div>
  
</div>

