
<div class="<?php print $classes ?> clearfix"
     id="node-<?php print $node->nid; ?>"
     <?php print $attributes; ?> >

<?php if ($page == 0): ?>
  <h2 class="title">
    <a href="<?php print $node_url ?>"><?php print $title; ?></a>
  </h2>
<?php endif; ?>

  <?php if ($user_picture) print $user_picture; ?>  
  
<?php if ($display_submitted) : ?>
  <span class="submitted">
    <?php print t('Posted !datetime', array('!datetime' => $date)); ?>
  </span> 
<?php endif; ?>

  <div class="content" <?php print $content_attributes; ?>>
    <?php print render($content); ?>
  </div>

</div>

